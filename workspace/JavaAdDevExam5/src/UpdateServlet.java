import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class StartAppServlet
 */
@WebServlet("/updateServlet")
public class UpdateServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // TODO Auto-generated method stub
        response.getWriter().append("Served at: ").append(request.getContextPath());
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        /*
         * 下記のコメントを参考に必要な処理を実装してください
         */

        // todo:入力値取得
    	request.setCharacterEncoding("UTF-8");

    	String speed = request.getParameter("speed");
    	String btn = request.getParameter("btn");




        // todo:戻るボタンクリック時、「input.jsp」へ遷移

    	if (btn.equals("back")) {
    		 request.getRequestDispatcher("input.jsp").forward(request, response);
    	}

        // todo:数値項目の入力値を数値に変換


        // セッションを取得
        HttpSession session = request.getSession();

        // セッションからinput.jspで作成したCarオブジェクトを取得
        Car car = (Car) session.getAttribute("car");

        // todo:セッターを使って、現在の速度(入力値)をセット


        // todo:メッセージをセット


        // 結果画面へ遷移
        request.getRequestDispatcher("update.jsp").forward(request, response);

    }
}
