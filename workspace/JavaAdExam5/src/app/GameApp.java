package app;

public abstract class GameApp implements App {

	private String item;

	private int playTime;


	//private String name;

	/*public String getItem() {
		return this.item;
	}*/

	/*public void setItem(String item) {
		this.item = item;
	}*/

	public String getItem() {
		return item;
	}

	public void setItem(String item) {
		this.item = item;
	}

	public int getPlayTime() {
		return playTime;
	}

	public void setPlayTime(int playTime) {
		this.playTime = playTime;
	}

	public GameApp() {

	}

	public GameApp(String item) {

		this.item = item;
	}

	protected String play() {
		return "";
	}

	public String start(String name) {
		return name + "さんと" + item + "でゲームを開始します。" + play();
	}

}
