
public class Cat extends Animal {

	public Cat() {
		// TODO 自動生成されたコンストラクター・スタブ
	}

	public Cat(String name, int age) {
		super(name, age);
		// TODO 自動生成されたコンストラクター・スタブ
	}

	@Override
	public String introduceAge() {
		String msg = null;
		// TODO 自動生成されたメソッド・スタブ
//		String msg = null;
		if (this.age == 0) {
				msg = "0～10代中盤";
		}else if (this.age == 1) {
			msg = "10代中盤";
		}else if (this.age == 2 || this.age == 3) {
			msg = "20代";
		}else if (this.age == 4 || this.age == 5) {
			msg = "30代";
		}else if (this.age >= 6 && this.age <= 8) {
			msg = "40代";
		}else if (this.age >= 9 && this.age <= 10) {
			msg = "50代";
		}else if (this.age > 10) {
			msg = "60歳以上";
		}

		//		switch (this.age) {
		//		case 0:
		//			msg = "0～10代中盤";
		//			break;
		//		case 1:
		//			msg = "10代中盤";
		//			break;
		//		case 2 OR 3:
		//}

		return "種類はネコです。人間で言うと、" + msg + "です。";
	}

}
