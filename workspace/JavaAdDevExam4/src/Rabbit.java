
public class Rabbit extends Animal {

	public Rabbit() {
		// TODO 自動生成されたコンストラクター・スタブ
	}

	public Rabbit(String name, int age) {
		super(name, age);
		// TODO 自動生成されたコンストラクター・スタブ
	}

	@Override
	public String introduceAge() {
		// TODO 自動生成されたメソッド・スタブ
		String msg = null;
		if (this.age == 0) {
				msg = "0～10代中盤";
		}else if (this.age == 1) {
			msg = "10代中盤";
		}else if (this.age == 2) {
			msg = "20代";
		}else if (this.age == 3) {
			msg = "30代";
		}else if (this.age == 4) {
			msg = "40代";
		}else if (this.age == 5) {
			msg = "50代";
		}else if (this.age >= 6) {
			msg = "60歳以上";
		}
		return "種類はウサギです。人間で言うと、" + msg + "です。";
	}

}
