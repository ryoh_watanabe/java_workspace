<%@ page language="java" contentType="text/html; charset=UTF-8"
  pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Java基礎_演習問題1(発展)</title>
</head>
<body>

  <h1>Java基礎 - 演習問題1(発展)</h1>

  <p>
    <span>ケーキの金額：\500円</span>
    <br>
    <span>コーヒーの金額：\350円</span>
    <br>
    <span>所持金：\3200円</span>
  </p>

  <%

  int cake = 500;
  int coffe = 350;
  int money = 3200;
  out.println("ケーキを3つとコーヒーを2つ購入する場合の金額は、" + "\\" + (cake * 3 + coffe * 2) + "です");
  out.println("<br>");
  out.println("ケーキを2つとコーヒーを3つ買った場合、所持金が" + "\\" + (money - (cake * 2 + coffe * 3)) + "残ります");
  out.println("<br>");
  out.println("所持金でケーキとコーヒーを" + money / (cake + coffe) + "セット購入できます");
  out.println("<br>");
  out.println("所持金でケーキを買えるだけ買った後に、\\1000増えた場合の金額は、\\" + (money % cake + 1000) + "です");
  %>
</body>
</html>