-- ①CREATE
CREATE TABLE sales_old (
    sales_id INT PRIMARY KEY,
    order_date DATE,
    customer_id INT NOT NULL REFERENCES customer(customer_id),
    amount DECIMAL);

-- ②INSERT
INSERT INTO sales_old
    VALUES
    (6, '2018/09/02', 2, 20000),
    (7, '2018/09/02', 1, 5000),
    (8, '2018/09/02', 3, 6000),
    (9, '2018/09/05', 1, 3000);

-- ③INSERT、SELECT
 INSERT INTO sales SELECT * FROM sales_old;

-- ④DROP
DROP TABLE sales_old;

-- ⑤CASE WHEN、別名
SELECT sales_id, order_date,
(CASE WHEN order_date < '2018-10-01' THEN '〇' ELSE '' END)
AS is_old FROM sales ORDER BY order_date;

-- ⑥文字結合、別名
SELECT customer_id || ':' || customer_name AS customer_id_name
FROM customer;

-- ⑦LIMIT
SELECT * FROM sales WHERE customer_id = 1
ORDER BY order_date DESC LIMIT 2;

-- ⑧GROUP BY、MIN、SUM、サブクエリ、別名
SELECT MIN(order_date), SUM(amount) AS sum_amount FROM sales
WHERE order_date IN(SELECT MIN(order_date) FROM sales);

-- ⑨GROUP BY、JOIN、AVG、TRUNC、別名
SELECT c.customer_id, c.customer_name, TRUNCATE(AVG(s.amount), 0)AS avg_amount
FROM customer c JOIN sales s
ON c.customer_id = s.customer_id
GROUP BY customer_id;

-- ⑩BETWEEN、LIMIT、サブクエリ
SELECT * FROM sales WHERE order_date IN
(SELECT order_date FROM sales
WHERE order_date BETWEEN '2018-09-01' AND '2018-09-30')
LIMIT 1;