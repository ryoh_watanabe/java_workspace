-- ①AND条件、並べ替え
SELECT * FROM student WHERE grade = 1 AND hometown = "東京";

-- ②OR条件、LIKE指定、並べ替え
SELECT * FROM student WHERE grade = 1 OR student_name LIKE '%本' ORDER BY student_id DESC;

-- ③GROUP BY、MAX
SELECT major_id, MAX(grade) FROM student GROUP BY major_id;

-- ④GROUP BY、COUNT、HAVING
SELECT hometown, COUNT(hometown) FROM student GROUP BY hometown  HAVING COUNT(hometown) >1;

-- ⑤JOIN、並べ替え
SELECT student.student_name, major.major_name
FROM student JOIN major ON student.major_id = major.major_id
ORDER BY major.major_name, student.student_name;

-- ⑥JOIN、条件指定、並べ替え
SELECT s.student_id, s.student_name, s.hometown, m.major_name
    FROM student s JOIN major m ON s.major_id = m.major_id
    WHERE s.hometown NOT IN ('東京')
    ORDER BY m.major_name, s.student_id;

-- ⑦サブクエリ - その1
SELECT student_id, student_name, grade FROM student
    WHERE major_id = (SELECT major_id FROM major WHERE major_id = 1);

-- ⑧サブクエリ - その2、IN
SELECT student_id, student_name, major_id FROM student
    WHERE major_id IN (SELECT major_id FROM student GROUP BY major_id HAVING COUNT(major_id) >2);
