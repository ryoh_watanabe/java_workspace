<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
	String sumTitle = "合計";
	String [] month = {"1月","2月","3月"};
	String [] productCode = {"商品", "飲料"};

	int amount[][];
	amount = new int[2][3];
	// amount[0][] = 食品
	amount [0][0] = 352000;
	amount [0][1] = 442000;
	amount [0][2] = 520000;
	// amount[1][] = 飲料
	amount [1][0] = 278000;
	amount [1][1] = 342600;
	amount [1][2] = 380500;

	int sumProductAmount [] = {amount[0][0]+amount[0][1]+amount[0][2],amount[1][0]+amount[1][1]+amount[1][2]};
	// int sumProductAmount [] = {for (int i = 0; i <= 2; i++){amount[0][] += amount[0][i]},amount[1][0]+amount[1][1]+amount[1][2]};

	int sumMonthAmount [] = {amount[0][0]+amount[1][0],amount[0][1]+amount[1][1],amount[0][2]+amount[1][2]};

	int total = sumMonthAmount[0] + sumMonthAmount[1] + sumMonthAmount[2];
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Java基礎_演習問題2(発展)</title>
<style>
table {
  border-collapse: collapse;
}

table th, table td {
  border: solid 1px black;
}
</style>
</head>
<body>
<h1>Java基礎 - 演習問題2</h1>
<h2>売上一覧</h2>
<table>
  <tr>
    <th></th>
    <th><%out.println(month[0]); %></th>
    <th><%out.println(month[1]); %></th>
    <th><%out.println(month[2]); %></th>
    <th><%out.println(sumTitle); %></th>
  </tr>
  <tr>
    <th><%out.println(productCode[0]); %></th>
    <th><%out.println(amount[0][0]); %></th>
    <th><%out.println(amount[0][1]); %></th>
    <th><%out.println(amount[0][2]); %></th>
    <th><%out.println(sumProductAmount[0]); %></th>
  </tr>
  <tr>
    <th><%out.println(productCode[1]); %></th>
    <th><%out.println(amount[1][0]); %></th>
    <th><%out.println(amount[1][1]); %></th>
    <th><%out.println(amount[1][2]); %></th>
    <th><%out.println(sumProductAmount[1]); %></th>
  </tr>
  <tr>
    <th><%out.println(sumTitle); %></th>
    <th><%out.println(sumMonthAmount[0]); %></th>
    <th><%out.println(sumMonthAmount[1]); %></th>
    <th><%out.println(sumMonthAmount[2]); %></th>
    <th><%out.println(total); %></th>
</table>

</body>
</html>