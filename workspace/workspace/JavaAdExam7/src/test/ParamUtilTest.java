package test;

import static org.junit.Assert.*;

import org.junit.Test;

import util.ParamUtil;

public class ParamUtilTest {

//1.isNullOrEmptyのテストケース
	@Test
	public void isNullOrEmpty入力データパターン1() {
		boolean result = ParamUtil.isNullOrEmpty("a");
		boolean expected = false;

		assertEquals (expected, result);
	}

	@Test
	public void isNullOrEmpty入力データパターン2() {
		boolean result = ParamUtil.isNullOrEmpty("");
		boolean expected = true;

		assertEquals (expected, result);
	}

	@Test
	public void isNullOrEmpty入力データパターン3() {
		boolean result = ParamUtil.isNullOrEmpty(null);
		boolean expected = true;

		assertEquals (expected, result);
	}



//2.isNumberのテストケース
	@Test
	public void isNumber入力データパターン1() {
		boolean result = ParamUtil.isNumber("-2147483649");
		boolean expected = false;

		assertEquals (expected, result);
	}

	@Test
	public void isNumber入力データパターン2() {
		boolean result = ParamUtil.isNumber("-2147483648");
		boolean expected = true;

		assertEquals (expected, result);
	}

	@Test
	public void isNumber入力データパターン3() {
		boolean result = ParamUtil.isNumber("0");
		boolean expected = true;

		assertEquals (expected, result);
	}

	@Test
	public void isNumber入力データパターン4() {
		boolean result = ParamUtil.isNumber("2147483647");
		boolean expected = true;

		assertEquals (expected, result);
	}

	@Test
	public void isNumber入力データパターン5() {
		boolean result = ParamUtil.isNumber("2147483648");
		boolean expected = false;

		assertEquals (expected, result);
	}

	@Test
	public void isNumber入力データパターン6() {
		boolean result = ParamUtil.isNumber(null);
		boolean expected = false;

		assertEquals (expected, result);
	}

	@Test
	public void isNumber入力データパターン7() {
		boolean result = ParamUtil.isNumber("");
		boolean expected = false;

		assertEquals (expected, result);
	}

	@Test
	public void isNumber入力データパターン8() {
		boolean result = ParamUtil.isNumber("a");
		boolean expected = false;

		assertEquals (expected, result);
	}

	@Test
	public void isNumber入力データパターン9() {
		boolean result = ParamUtil.isNumber("１");
		boolean expected = true;

		assertEquals (expected, result);
	}

	@Test
	public void isNumber入力データパターン10() {
		boolean result = ParamUtil.isNumber("あ");
		boolean expected = false;

		assertEquals (expected, result);
	}


//3.checkAndParseIntのテストケース
	@Test
	public void checkAndParseInt入力データパターン1() {
		Integer result = ParamUtil.checkAndParseInt("-2147483649");
		Integer expected = null;

		assertEquals (expected, result);
	}

	@Test
	public void checkAndParseInt入力データパターン2() {
		Integer result = ParamUtil.checkAndParseInt("-2147483648");
		Integer expected = -2147483648;

		assertEquals (expected, result);
	}

	@Test
	public void checkAndParseInt入力データパターン3() {
		Integer result = ParamUtil.checkAndParseInt("0");
		Integer expected = 0;

		assertEquals (expected, result);
	}

	@Test
	public void checkAndParseInt入力データパターン4() {
		Integer result = ParamUtil.checkAndParseInt("2147483647");
		Integer expected = 2147483647;

		assertEquals (expected, result);
	}

	@Test
	public void checkAndParseInt入力データパターン5() {
		Integer result = ParamUtil.checkAndParseInt("2147483648");
		Integer expected = null;

		assertEquals (expected, result);
	}

	@Test
	public void checkAndParseInt入力データパターン6() {
		Integer result = ParamUtil.checkAndParseInt(null);
		Integer expected = null;

		assertEquals (expected, result);
	}

	@Test
	public void checkAndParseInt入力データパターン7() {
		Integer result = ParamUtil.checkAndParseInt("");
		Integer expected = null;

		assertEquals (expected, result);
	}

	@Test
	public void checkAndParseInt入力データパターン8() {
		Integer result = ParamUtil.checkAndParseInt("a");
		Integer expected = null;

		assertEquals (expected, result);
	}

	@Test
	public void checkAndParseInt入力データパターン9() {
		Integer result = ParamUtil.checkAndParseInt("１");
		Integer expected = 1;

		assertEquals (expected, result);
	}

	@Test
	public void checkAndParseInt入力データパターン10() {
		Integer result = ParamUtil.checkAndParseInt("あ");
		Integer expected = null;

		assertEquals (expected, result);
	}
}