package entity;

/**
 * Carクラス
 */
public class Car {

private String carName;
private String bodyColor;
private int speed;
private int maxSpeed;


public String getCarName() {
	return carName;
}

public void setCarName(String carName) {
	this.carName = carName;
}

public String getBodyColor() {
	return bodyColor;
}

public void setBodyColor(String bodyColor) {
	this.bodyColor = bodyColor;
}

public int getSpeed() {
	return speed;
}

public void setSpeed(int speed) {
	this.speed = speed;
}

public int getMaxSpeed() {
	return maxSpeed;
}

public void setMaxSpeed(int maxSpeed) {
	this.maxSpeed = maxSpeed;
}


public Car() {

}

public Car(String carName, String bodyColor, int maxSpeed) {
this.setCarName(carName);
this.setBodyColor(bodyColor);
this.setMaxSpeed(maxSpeed);
}

public Car(String carName, String bodyColor, int maxSpeed, int speed) {

}
}
