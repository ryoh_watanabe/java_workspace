package servlet;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import entity.Car;
import util.Utility;

/**
 * Servlet implementation class StartAppServlet
 */
@WebServlet("/inputServlet")
public class InputServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public InputServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // TODO Auto-generated method stub
        response.getWriter().append("Served at: ").append(request.getContextPath());
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        /*
         * 下記のコメントを参考に必要な処理を実装してください
         */

    	String result = null;
        // todo:入力値取得
    	request.setCharacterEncoding("UTF-8");
    	String carName = request.getParameter("carName");
    	String bodyColor = request.getParameter("bodyColor");
    	String maxSpeed = request.getParameter("maxSpeed");


        // todo:未入力チェック
        // (入力値の3つのうち、どれか一つでも未入力なら「input.jsp」へ戻る
        //  未入力かどうかの判定は、UtilityクラスのisNullOrEmptyメソッドを使用)
//    	Utility util = new Utility();
    	if (Utility.isNullOrEmpty(carName) || Utility.isNullOrEmpty(bodyColor) || Utility.isNullOrEmpty(maxSpeed)) {
    		result = "未入力の項目があります。";
    		request.setAttribute("result", result);
    		request.getRequestDispatcher("input.jsp").forward(request, response);
    	}
//    	else if (!Utility.isNullOrEmpty(carName) && !Utility.isNullOrEmpty(bodyColor) && !Utility.isNullOrEmpty(maxSpeed)) {
//    		result = "入力項目全部入れられてえらい！";
//    		request.setAttribute("result", result);
//    		request.getRequestDispatcher("input.jsp").forward(request, response);
//    	}



        // todo:数値に変換
        // (数値項目[最高速度]の入力値の値を数値に変換)
    	int intmSpeed = Integer.parseInt(maxSpeed);

        // todo:Carオブジェクト作成
        // 現在はnullをセットしている。Carオブジェクトを作成し、
        // フィールドに入力値をセットする。
        Car car = new Car();
        car.setCarName(carName);
        car.setBodyColor(bodyColor);
        car.setMaxSpeed(intmSpeed);

        // セッションを取得
        HttpSession session = request.getSession();

        // セッションにCarオブジェクトをセット
        session.setAttribute("car", car);

        // 結果画面へ遷移
        request.getRequestDispatcher("update.jsp").forward(request, response);

    }
}
