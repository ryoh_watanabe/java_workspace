package jp.co.axiz.entity;

/**
 *
 * 商品用クラス
 *
 */
public class Product {

	private String productName;
	private Integer price;

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public Integer getPrice() {
		return price;
	}

	public void setPrice(Integer price) {
		this.price = price;
	}

	public Product() {

	}

	public Product(String productName, Integer price) {
		this.setProductName(productName);
		this.setPrice(price);
	}
}
