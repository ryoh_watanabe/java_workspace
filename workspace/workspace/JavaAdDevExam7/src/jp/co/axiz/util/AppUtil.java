package jp.co.axiz.util;

public class AppUtil {

	public static boolean isGameApp(Object app) {
		if (app instanceof GameApp) {
			return true;
		}else {
			return false;
		}
	}

	public static boolean isCardGameApp(Object app) {
		if (app instanceof CardGameApp) {
			return true;
		}else {
			return false;
		}
	}

	public static boolean isDartsGameApp(Object app) {
		if (app instanceof DartsGameApp) {
			return true;
		}else {
			return false;
		}
	}

	public static boolean isClockApp(Object app) {
		if (app instanceof ClockApp) {
			return true;
		}else {
			return false;
		}
	}

	public static String getAppName(Object app) {

		String name_f = null;
		String name_b = null;
		String result = null;

		if (app instanceof GameApp) {
			name_f = "ゲーム:";

			if (app instanceof CardGameApp) {
				name_b = "カード";
			}
			else if (app instanceof DartsGameApp) {
				name_b = "ダーツ";
			}
			result = name_f + name_b;
		}


		else if (app instanceof ClockApp) {
			result = "時計";
		}
		return result;
	}
}
