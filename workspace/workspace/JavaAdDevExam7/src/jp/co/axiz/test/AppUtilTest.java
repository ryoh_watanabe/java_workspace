package jp.co.axiz.test;

import static org.junit.Assert.*;

import org.junit.Test;

import jp.co.axiz.util.AppUtil;
import jp.co.axiz.util.CardGameApp;
import jp.co.axiz.util.ClockApp;
import jp.co.axiz.util.DartsGameApp;;

public class AppUtilTest {

	// isGameAppのテストケース
	@Test
	public void isGameAppパターン1() {
		CardGameApp card = new CardGameApp();
		boolean result = AppUtil.isGameApp(card);
		boolean expected = true;

		assertEquals (expected, result);
		}

	@Test
	public void isGameAppパターン2() {
		DartsGameApp darts = new DartsGameApp();
		boolean result = AppUtil.isGameApp(darts);
		boolean expected = true;

		assertEquals (expected, result);
		}

	@Test
	public void isGameAppパターン3() {
		ClockApp clock = new ClockApp();
		boolean result = AppUtil.isGameApp(clock);
		boolean expected = false;

		assertEquals (expected, result);
		}

	// isCardGameAppのテストケース
	@Test
	public void isCardGameAppパターン1() {
		CardGameApp app = new CardGameApp();
		boolean result = AppUtil.isCardGameApp(app);
		boolean expected = true;

		assertEquals (expected, result);
	}

	@Test
	public void isCardGameAppパターン2() {
		DartsGameApp app = new DartsGameApp();
		boolean result = AppUtil.isCardGameApp(app);
		boolean expected = false;

		assertEquals (expected, result);
	}

	@Test
	public void isCardGameAppパターン3() {
		ClockApp app = new ClockApp();
		boolean result = AppUtil.isCardGameApp(app);
		boolean expected = false;

		assertEquals (expected, result);
	}

	// isDartsGameAppのテストケース
	@Test
	public void isDartsGameAppパターン1() {
		CardGameApp app = new CardGameApp();
		boolean result = AppUtil.isDartsGameApp(app);
		boolean expected = false;

		assertEquals (expected, result);
	}

	@Test
	public void isDartsGameAppパターン2() {
		DartsGameApp app = new DartsGameApp();
		boolean result = AppUtil.isDartsGameApp(app);
		boolean expected = true;

		assertEquals (expected, result);
	}

	@Test
	public void isDartsGameAppパターン3() {
		ClockApp app = new ClockApp();
		boolean result = AppUtil.isDartsGameApp(app);
		boolean expected = false;

		assertEquals (expected, result);
	}

	// isClockAppのテストケース
	@Test
	public void isClockAppパターン1() {
		CardGameApp app = new CardGameApp();
		boolean result = AppUtil.isClockApp(app);
		boolean expected = false;

		assertEquals (expected, result);
	}

	@Test
	public void isClockAppパターン2() {
		DartsGameApp app = new DartsGameApp();
		boolean result = AppUtil.isClockApp(app);
		boolean expected = false;

		assertEquals (expected, result);
	}

	@Test
	public void isClockAppパターン3() {
		ClockApp app = new ClockApp();
		boolean result = AppUtil.isClockApp(app);
		boolean expected = true;

		assertEquals (expected, result);
	}

	// getAppNameのテストケース
	@Test
	public void getAppNameパターン1() {
		CardGameApp app = new CardGameApp();
		String result = AppUtil.getAppName(app);
		String expected = "ゲーム:カード";

		assertEquals (expected, result);
	}

	@Test
	public void getAppNameパターン2() {
		DartsGameApp app = new DartsGameApp();
		String result = AppUtil.getAppName(app);
		String expected = "ゲーム:ダーツ";

		assertEquals (expected, result);
	}

	@Test
	public void getAppNameパターン3() {
		ClockApp app = new ClockApp();
		String result = AppUtil.getAppName(app);
		String expected = "時計";

		assertEquals (expected, result);
	}


}
