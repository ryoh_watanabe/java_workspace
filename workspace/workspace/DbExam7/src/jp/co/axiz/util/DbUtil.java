package jp.co.axiz.util;

import java.sql.Connection;
import java.sql.DriverManager;

public class DbUtil {
	public static Connection getConnection() {
		// Connection con = null;
		try {
			Class.forName("org.postgresql.Driver");
			//con = DriverManager.getConnection("jdbc:postgresql://localhost:5432/dbconnection", "postgres", "postgres");
			//con.setAutoCommit(false);
			return /*con*/ DriverManager.getConnection("jdbc:postgresql://localhost:5432/dbconnection", "postgres", "postgres");
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

}
