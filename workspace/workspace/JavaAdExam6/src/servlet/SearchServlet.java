package servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class StartAppServlet
 */
@WebServlet("/SearchServlet")
public class SearchServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public SearchServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		/*
		  ※検索ボタンを押したときに必要な処理を実装してください。
		*/
		request.setCharacterEncoding("UTF-8");

		String result = null;

		//if(search.isEmpty() || search == null) {
			//String result = "見つかりませんでした";
			//request.setAttribute("result", result);
			//request.getRequestDispatcher("dictionary.jsp").forward(request, response);
	//}
		try {
			String english = request.getParameter("english");

			String search = Dictionary.dictionary.get(english);

			// 見つかった時にだけ結果を変数に入れる。じゃないと見つからなくてもnullを入れちゃうから
			//catchのメッセージを入れられない。
			if (!search.isEmpty() || search != null) {
			result = search;
			}
		}catch ( NullPointerException e) {
			result = "見つかりませんでした";

		}

		request.setAttribute("result", result);
		request.getRequestDispatcher("dictionary.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}
}
