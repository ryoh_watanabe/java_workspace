
<%@ page import="app.GameApp"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	/*request.setCharacterEncoding("UTF-8");
	String name = request.getParameter("name");

	String result = "未実施";

	if (name != null && !name.isEmpty()) {
		// このif分の中に、GameAppクラスを使った下記処理を記載してください。


		//   startメソッドの引数には、ユーザ名テキストボックスの入力を渡す

		// インストラクター作成
		GameApp app = new GameApp();

		// ・GameAppクラスのオブジェクトを作成し、itemフィールドに "何か" という文字列をセット
		app.setItem ("何か");

		// ・GameAppオブジェクトのstartメソッドを呼び、戻り値を変数resultへ代入する
		String reqName = request.getParameter("name");
		result = app.start(reqName);

	}*/
	String result = (String) request.getAttribute("result");
	String strTime = (String) request.getAttribute("strTime");
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Java応用_演習問題1</title>
<style>
body {
	border: solid 2px #000080;
	padding: 5px;
}

.result {
	background: #fffacd;
	display: inline-block;
	margin: 5px;
	padding: 10px;
}
</style>
</head>
<body>

	<h1>Java応用 - 演習問題1</h1>

	<h2>ゲームアプリ実行ページ</h2>
<c:if test="${not empty result}">
	<div class="result">
		<h3>アプリの実行結果</h3>
		<p><%=result%></p>
		<c:if test="${not empty strTime}">
		<p>play時間:<%=strTime%>分</p>
		</c:if>
	</div>
</c:if>
	<form action="StartAppServlet" method="post">
		<label>ユーザ名：</label> <input type="text" name="name"> <br>
		<label>アプリ：</label> <input type="radio" name="item" value="card" checked>トランプ
		<input type="radio" name="item" value="darts">ダーツ
		<input type="radio" name="item" value="clock">時計
		<input type="radio" name="item" value="other">その他<br>
		<button type="submit">実行</button>
	</form>
</body>
</html>